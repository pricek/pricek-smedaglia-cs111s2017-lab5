 
//***********************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Kierra Price and Sam Medaglia
// CMPSC 111 Spring 2017
// Lab #5
// Date: 2/23/17
//
// Purpose: To generate a word search with user input 
//***********************************
import java.util.Date; // needed for printing today's date
import java.util.Scanner;

public class WordHide
{
	public static void main(String[] args) // main method starts here
	{
		// Label output with name and date:
		System.out.println("Kierra Price and Sam Medaglia\nLab 5\n" + new Date() + "\n");
		
		
		String message;
		Scanner scan = new Scanner(System.in);
		System.out.println("Please enter at least a 10 letter word:"); //has user input a ten letter word
		message = scan.nextLine();
		message = message.toUpperCase(); //capitalizes the user inputs
		message = message.substring(0,10); //cuts off word at ten characters

		// declaring chars 
		char A = message.charAt(0);
		char B = message.charAt(1);
		char C = message.charAt(2);
		char D = message.charAt(3);
		char E = message.charAt(4);
		char F = message.charAt(5);
		char G = message.charAt(6);
		char H = message.charAt(7);
		char I = message.charAt(8);
		char J = message.charAt(9);


		// Start of the word search
		// the word is up-side-down in the 11th row
		System.out.println("FJBD" + A + F + C + "KIMLP" + J + F + E + H + "BAOG"); //line 1
		System.out.println("" + G + E + F + J + I + "WMS" + A + B + C + "ILMOD" + F + J + D + C); //line 2, empty string at beginning
		System.out.println("XHIVOPCAQU" + H + G + E + I + A + "JIFE" + A); //line 3
		System.out.println("KITEBH" + A + C + I + J + "AJUIFRB" + J + D + B); //line 4
		System.out.println("MNEALE" + B + E + D + J + J + I + "HGIJKER" + C); //line 5, last letter of word starts @ 11th char
		System.out.println("DET" + G + C + I + "JETP" + I + I + H + J + I + H + "KAT" + D); //line 6, second @ 11th
		System.out.println("METROLLI" + A + J + H + H + A + "NATLIFE"); //line 7, third @ 11th
		System.out.println("" + J + D + E + H + "REDEW" + J + G + I + G + A + B + C + "THEP"); //line 8, fourth @ 11th
		System.out.println("ANITME" + A + D + "TH" + F + H + F + I + "LAIT" + B + E); //line 9, fifth @ 11th
		System.out.println("LEFT" + B + "VS" + G + G + F + E + E + J + A + "BEHTAP"); //line 10, SIXTH @ 11TH
		System.out.println("" + A + B + D + E + "KALEHA" + D + E + D + F + "TEDWAX"); //line 11, SEVENTH @ 11TH
		System.out.println("" + B + A + D + F + "HENTA" + C + C + E + D + H + "LOINPT"); //line 12, EIGHTH @ 11TH
		System.out.println("" + C + D + E + "NAIETN" + A + B + D + C + F + "QUSIBO"); //line 13, NINETH @ 11TH
		System.out.println("" + D + E + F + J + "AYEO" + B + C + A + A + C + F + "PLAR" + B + A); //line 14, TENTH @ 11TH
		System.out.println("" + E + E + A + "FAITN" + B + C + D + A + E + F + "FISHTE"); //line 15
		System.out.println("" + F + J + I + H + G + F + "FETAHEOB" + B + C + G + H + I + A); //line 16
		System.out.println("" + G + "KNODE" + J + E + J + I + H + G + F + C + "NITSOA"); //line 17
		System.out.println("" + I + H + "PLIDSE" + A + B + C + D + E + "MAIDEGS"); //line 18
		System.out.println("" + J + E + B + "SHOETS" + B + C + A + I + G + "NIGTA" + B); //line 19
		System.out.println("BISDFGEU" + G + B + A + C + I + "LJAIE" + A + B); //line 20


	}
}
